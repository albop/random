The goal of this course, is to provide an introduction to modern scientific Programming using open source tools (linux, git, julia).
In addition to a short formal exposition of important programming concepts (types, multiple dispatch, vectorization/parallelization), it introduces several topics from data analysis as motivational devices. The requirement is some prior experience with programming (for instance Matlab), but no formal computational background. It is a hands-on course, where students are expected to bring their laptop.

Topics:
- General introduction to Julia Programming
- Network analysis
        Use REPEC citation data to analyze strands of literature.
- Text analysis
        Scrape the web, extract information from webpages and texts. Sentiment analysis.
- Classifying/clustering data
        Train a neural network to classify data.
- Big Data (Out of core computation.)
        Use AWS to implement big regressions online.


Proposed duration: 5 times 3 hours, or 4 times 4.

Evaluation: could be one of the following options (tbd)
- make a contribution to an opensource library (to be discussed with teacher: can be dolo, quantecon, dyanre, any other OS lib)
- present a small data analysis project
- participate to a hackaton
- a programming test (one hour to solve some algorithmic problems)
