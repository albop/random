using JSON

jj = JSON.parsefile("gimf_steady3.json")

model_eqs = [(string(ee["lhs"]), string(ee["rhs"])) for ee in jj["model"]]
model_eqs

eq_stmt = model[1]

function parse_eq(d::Tuple{String, String})
    lhs = parse(d[1])
    rhs = parse(d[2])
    eqd = :($rhs - $lhs)
end


@time equations = [parse_eq(eq) for eq in model_eqs]


@time endogenous = [Symbol(e["name"]) for e in jj["endogenous"]]

@time exogenous = [Symbol(e["name"]) for e in jj["endogenous"]]
@time parameters = [Symbol(e["name"]) for e in jj["parameters"]]

import Dolang
@time it = Dolang.IncidenceTable(equations)

variables = cat(1,endogenous, exogenous)

v_args = cat(1,
            [(v,1) for v in variables if v in it.by_date[1]],
            [(v,0) for v in variables if v in it.by_date[0]],
            [(v,-1) for v in variables if v in it.by_date[-1]],
            [(v,0) for v in exogenous if v in it.by_date[0]]
            )


pvec = [(p) for p in parameters]
vvec = cat(1,var_future, var_present, var_past)

calibration = Dict{Symbol, Float64}()

for e in jj["statements"]
    if e["statementName"] == "param_init"
        name = e["name"]
        # val = parse(Float64, (e["value"]))
        val = parse(Float64, strip(strip(e["value"],'('),')'))
        calibration[Symbol(name)] = val
    end
end
using DataStructures
symbols = Dict(
    :endogenous=>endogenous,
    :exogenous=>exogenous,
    :parameters=>parameters
)

calibration_grouped = OrderedDict{Symbol,Array{Float64,1}}()
for vg in [:endogenous,:exogenous,:parameters]
    if vg == :parameters
        default = NaN
    else
        default = 0.0
    end
    calibration_grouped[vg] =  [get(calibration,v,default) for v in symbols[vg]]
end



for v in variables
    calibration[v] = 0.0
end






ff = Dolang.FunctionFactory(equations, vvec, pvec, funname=:fun)
@time code = Dolang.make_function(ff)


function get_vectors(symbols, equations, calibration_grouped)


    n_v = length(symbols[:endogenous])
    n_e = length(symbols[:exogenous])

    endogenous = symbols[:endogenous]
    exogenous = symbols[:exogenous]
    parameters = symbols[:parameters]

    it = Dolang.IncidenceTable(equations)

    fut_syms = it.by_date[1]
    cur_syms = it.by_date[0]
    past_syms = it.by_date[-1]

    fut_inds = [i for (i,v) in enumerate(endogenous) if v in it.by_date[1]]
    cur_inds = [i for (i,v) in enumerate(endogenous) if v in it.by_date[0]]
    past_inds = [i for (i,v) in enumerate(endogenous) if v in it.by_date[-1]]
    shocks_inds = [i for (i,v) in enumerate(exogenous) if v in it.by_date[0]]
    # parms_inds = [i for (i,v) in enumerate(parameters) if v in it.by_date[0]]
    parms_inds = 1:length(symbols[:parameters])

    y_ss = calibration_grouped[:endogenous]
    e_ss = calibration_grouped[:exogenous]
    p_ss = calibration_grouped[:parameters]

    v = cat(1, y_ss[fut_inds], y_ss[cur_inds], y_ss[past_inds], e_ss[shocks_inds])
    p = p_ss[parms_inds]

    return v,p

    # check residuals are zero
    # res = (f(Der{0}, v,p))
    # assert(maximum(abs(res))<1e-8)

    # jac = f(Der{1}, v,p)
end


v, p = get_vectors(symbols, equations, calibration_grouped)

import Dolang: Der
eval(Dolang, code)

@time res = fun(v,p)
