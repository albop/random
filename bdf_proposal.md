The goal of this proposal is to construct an experimental software to run a model forecast, using Julia and the Jupyter notebook frameworks. This software would:
1/ perform the solution of a very simple nonstationary structural model
2/ download exogenous data from available database and integrate it with manual judgment
3/ provide an interactive interface to explore the results along several dimensions (aka simulation dashboard)

As a byproduct, the development would produce two self-contained pieces of code, which are interesting in itself:
1/ A flexible algorithm to solve for the steady-state of nonlinear models, using both a block decomposition analysis of the model and manually supplied information about preferred decomposition. If successful, this algorithm could be translated in Matlab.
2/ A visualization module for simulated data, using Jupyter widgets. This would allow to explore both IRFs and stochastic simulations making it easy to select relevant variables and compare model variants. Ideally, the result could be exported into a self contained, interactive webpage.
