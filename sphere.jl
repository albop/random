module DevMod
    struct Point
        θ::Float64
        φ::Float64
    end

    function d(p1::Point, p2::Point)
        Δφ = p1.φ-p2.φ
        θ1 = p1.θ
        θ2 = p2.θ
        s = sin(θ1)*sin(θ2) + cos(θ1)*cos(θ2)*cos(Δφ)
        acos(s)
    end

    function cartesian(p::Point)
        x = sin(p.θ)*cos(p.φ)
        y = sin(p.θ)*sin(p.φ)
        z = cos(p.θ)
        (x,y,z)
    end

    function initial_distribution(N::Int)
        w = [linspace(0,1,N+1)...][1:end-1]
        phis =   -pi + rand(N)*2pi
        thetas = -pi/2 + rand(N)*pi
        return [(Point(phis[i],thetas[i])) for i=1:length(phis)]
    end

    function energy(points::Vector{Point})
        H = 0.0
        N = length(points)
        for i=1:N-1
            p1 = points[i]
            for j=1:(i-1)
                p2 = points[j]
                r = d(p1,p2)
                H += 1.0/r
            end
        end
        return H
    end


    flatten(dist::Vector{Point}) = [[e.θ for e in dist] ; [e.φ for e in dist]]

    function unflatten(sol::Vector{Float64})
        N = div(length(sol), 2)
        [Point(sol[1:N][i], sol[(N+1):end][i]) for i=1:N]
    end

    using PlotlyJS



    function visualize_points(dist::Vector{Point})
        cpoints = [cartesian(e) for e in dist]
        x = [e[1] for e in cpoints]
        y = [e[2] for e in cpoints]
        z = [e[3] for e in cpoints]
        trace = scatter3d(x=x,y=y,z=z, marker_line_width=0)
        data = GenericTrace[]
        push!(data, trace)

            # notice the nested attrs to create complex JSON objects
        layout = Layout(width=800, height=550, autosize=false, title="Iris dataset",
                    scene=attr(xaxis=attr(gridcolor="rgb(255, 255, 255)",
                                          zerolinecolor="rgb(255, 255, 255)",
                                          showbackground=true,
                                          backgroundcolor="rgb(230, 230,230)"),
                               yaxis=attr(gridcolor="rgb(255, 255, 255)",
                                           zerolinecolor="rgb(255, 255, 255)",
                                           showbackground=true,
                                           backgroundcolor="rgb(230, 230,230)"),
                               zaxis=attr(gridcolor="rgb(255, 255, 255)",
                                           zerolinecolor="rgb(255, 255, 255)",
                                           showbackground=true,
                                           backgroundcolor="rgb(230, 230,230)"),
                               aspectratio=attr(x=1, y=1, z=0.7),
                               aspectmode = "manual"))
        data, layout
    end
end
