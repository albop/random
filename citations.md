Si les vieilles voutes
Ne s'écroulent pas
C'est que leurs pierres
Veulent tomber toutes a la fois

(Julien Clerc: le secret de l'amour)

Si un jour quelqu'un te dit que je suis mort
Ne le crois pas ce croque-mort
Mais aime-moi un peu plus fort
Ton amour, j'en aurai ce jour-là besoin
Bien plus encore que de chagrins
Bien plus encore que ce matin

Cela dépend de toi
Que je survive ou pas...

(Serge Reggiani, Tu vivras tant qu'on t'aimera)


(Contre quelle vie me suis-je battu
Pour qu’on me cueille un matin
Des gens ont dit qu’ils m’avaient vu
Avec une arme dans la main
Les rêves à l’air et la tête en morceau
Ils m’ont jetés dans ce ghetto
A Villejuif section Colin)

Villejuif, Reggiani


"De deux choses l'une. L'autre c'est le soleil."
Julien Clerc: "Mais ces mots là ća me console..."


The past is impredictible.
Russian saying (Fargo)


"Si j’avais su que je l’aimais autant, je l’aurais aimé davantage."
Frederic Dard.

"Look for the bare necessities, the simple bare necessities. Forget about your worries and your stress."


\begin{Bonmot}{Victor Hugo}
  L’homme a reçu de la nature une clef\\
  avec laquelle il remonte la femme\\
  toutes les vingt-quatre heures.
\end{Bonmot}

Anger serves our ennemy. (Captain Marvel)

La science, c'est ce que le père enseigne à son fils. La technologie, c'est ce que le fils enseigne à son papa.
Read more at http://citation-celebre.leparisien.fr/citation/technologie#YFKIZErzPkzX5zFo.99

Michel Serres

“Lord, grant me the strength to accept the things I cannot change,
the courage to change the things I can,
and the wisdom to know the difference.”

― Reinhold Niebuhr



La souffrance enfante les songes
Comme une ruche ses abeilles
L'homme crie où son fer ronge
Et sa plaie engendre un soleil
Plus beaux que les anciens mensonges

Aragon (prologue)


La lucidité est la blessure la plus rapprochée du soleil.

René Char
