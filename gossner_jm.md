Bonjour Olivier,

Romain m'a suggéré de t'écrire. En un mot, je recherche un poste académique à Paris, après 4 ans comme chercheur au FMI et à la Banque d'Angleterre.

Sais-tu si le département de Polytechnique a des postes vacants et serait potentiellement intéressé par mon profil ? Ou s'il pourrait être mon laboratoire d'accueil si je réussis le concours du CNRS ?

Je ne suis pas sûr de t'avoir beaucoup parlé de mes recherches lorsqu'on s'est croisés à PSE il y a longtemps. Je joins un CV mais pour résumer, je dirais que je suis un macroécomiste avec une composante (très) computationnelle. Du point de vue appliqué, mes recherchent portent principalement sur la macrofinance internationale (intégration financière, réserves de change) et les  inégalités, plus récemment la politique optimale (commitment/discretion). Mes travaux méthodologiques concernent la resolution de modèles et sont assez transversaux (programmation dynamique, reinforcement learning, agents hétérogènes).

Sans vouloir être embarassant, j'aimerais bien avoir ton avis, et suis disponible pour en discuter n'importe quand.

A bientôt,

Pablo
