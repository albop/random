using Blink
using PlotlyJS
using Optim


include("sphere.jl")

import DevMod: initial_distribution, energy, cartesian, Point, flatten, unflatten, visualize_points, d







function visualize_points_2(dist::Vector{Point})

    cpoints = [cartesian(e) for e in dist]
    x = [e[1] for e in cpoints]
    y = [e[2] for e in cpoints]
    z = [e[3] for e in cpoints]
    trace = scatter3d(x=x,y=y,z=z, mode="markers", marker=attr(color="red"))


    N = 32
    u = linspace(0, 2π, N)
    v = linspace(0, π, N)
    x = cos(u) * sin(v)'
    y = sin(u) * sin(v)'
    z = repeat(cos(v)',outer=[N, 1])



    data = GenericTrace[]

    push!(data, surface(x=x,y=y,z=z, opacity=0.8))
    push!(data, trace)

        # notice the nested attrs to create complex JSON objects
    layout = Layout(width=800, height=550, autosize=false, title="Iris dataset",
                scene=attr(xaxis=attr(gridcolor="rgb(255, 255, 255)",
                                      zerolinecolor="rgb(255, 255, 255)",
                                      showbackground=true,
                                      backgroundcolor="rgb(230, 230,230)"),
                           yaxis=attr(gridcolor="rgb(255, 255, 255)",
                                       zerolinecolor="rgb(255, 255, 255)",
                                       showbackground=true,
                                       backgroundcolor="rgb(230, 230,230)"),
                           zaxis=attr(gridcolor="rgb(255, 255, 255)",
                                       zerolinecolor="rgb(255, 255, 255)",
                                       showbackground=true,
                                       backgroundcolor="rgb(230, 230,230)"),
                           aspectratio=attr(x=1, y=1, z=0.7),
                           aspectmode = "manual"))
    data, layout
end


function distance(a::Tuple{Float64,Float64,Float64}, b::Tuple{Float64,Float64,Float64})
    x1,y1,z1=a
    x2,y2,z2=b
    sqrt((x1-x2)^2+(y2-y1)^2+(z2-z1)^2)
end

import DevMod: cartesian

function energy2(points::Vector{Point})
    H = 0.0
    N = length(points)
    for i=1:N
        p1 = points[i]
        c1 = cartesian(p1)
        for j=1:N
            if i!=j
                p2 = points[j]
                c2 = cartesian(p2)
                # r = d(p1,p2)
                r = distance(c1,c2)
                H += 1.0/r
            end
        end
    end
    return H
end
obj2(v::Vector{Float64}) = energy2(unflatten(v))



N = 126
dist = initial_distribution(N)
# dat,lay = visualize_points_2(dist)
# plot(dat, lay)


v0 = flatten(dist)
obj2(v0)

result1 = Optim.optimize(obj2, v0, GradientDescent(), Optim.Options(store_trace=true, iterations=100))

Optim.converged(result1)
# # #
# result2 = Optim.optimize(obj2, result1.minimizer,(), Optim.Options(store_trace=true, iterations=100))
# Optim.converged(result2)
#
# dd1 = [e.iteration for e in result.trace]
# dd2 = [length(dd1)+e.iteration for e in result2.trace]
# dd = [dd1;dd2]
# vals = [[log10(e.value) for e in result.trace]; [log10(e.value) for e in result2.trace]]
# # #

using Plots
dd = [e.iteration for e in result1.trace]
vals = [log10(e.value) for e in result1.trace]

Plots.plot(dd,vals)

pl2 = unflatten(result1.minimizer)

dat,lay = visualize_points_2(pl2)
plot(dat, lay)


using JLD
save("solution.jld", "points", pl2)


function find_neighbours(points, point)
    ss = sort(points, by=x->DevMod.d(x,point))
    return ss[2:7]
end



ppoints = pl2
pp = ppoints[1]


find_neighbours(ppoints, pp)
